package com.example.vicktor.engineeringidea.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.vicktor.engineeringidea.model.CardModel;
import com.example.vicktor.engineeringidea.model.Friend;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "card_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Friends");
        db.execSQL("DROP TABLE IF EXISTS Cards");
        db.execSQL("DROP TABLE IF EXISTS Tags");
        createTables(db);
    }

    private void createTables(SQLiteDatabase db) {
        db.execSQL("create table Friends"
                + " ("
                + "cardId text,"
                + "id text,"
                + "name text"
                + ");");

        db.execSQL("create table Cards"
                + " ("
                + "id text,"
                + "isActive integer,"
                + "balance text,"
                + "picture text,"
                + "age integer,"
                + "eyeColor text,"
                + "name text,"
                + "gender text,"
                + "company text,"
                + "email text,"
                + "phone text,"
                + "address text,"
                + "about text,"
                + "registered text,"
                + "favoriteFruit text"
                + ");");

        db.execSQL("create table Tags"
                + " ("
                + "cardId text,"
                + "cardTags text"
                + ");");
    }

    public void saveCards(List<CardModel> cardModels) {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(CardsTable.TABLE_NAME, null, null);
        database.delete(TagsTable.TABLE_NAME, null, null);
        database.delete(FriendsTable.TABLE_NAME,null, null);
        for (CardModel cardModel : cardModels) {
            saveCard(cardModel);
        }
        database.close();
    }

    private void saveCard(CardModel cardModel) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = CardsTable.toContentValues(cardModel);
        database.insert(CardsTable.TABLE_NAME, null, contentValues);

        saveCardTags(cardModel);
        saveFriends(cardModel);
    }

    private void saveCardTags(CardModel cardModel) {
        SQLiteDatabase database = getWritableDatabase();
        List<String> cardTags = cardModel.getTags();
        for (String cardTag : cardTags) {
            ContentValues contentValues = TagsTable.toContentValues(cardModel.getId(), cardTag);
            database.insert(TagsTable.TABLE_NAME, null, contentValues);
        }
    }

    private void saveFriends(CardModel cardModel) {
        SQLiteDatabase database = getWritableDatabase();
        List<Friend> friends = cardModel.getFriends();
        for (Friend friend : friends) {
            ContentValues contentValues = FriendsTable.toContentValues(cardModel.getId(), friend);
            database.insert(FriendsTable.TABLE_NAME, null, contentValues);
        }
    }

    public List<CardModel> getCards() {
        List<CardModel> cardModels = new ArrayList<>();
        SQLiteDatabase database = getWritableDatabase();
        Cursor cursor = database.query(CardsTable.TABLE_NAME, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                CardModel cardModel = CardsTable.fromCursor(cursor);
                cardModels.add(cardModel);
            } while (cursor.moveToNext());
        }
        setCardTags(cardModels);
        setFriends(cardModels);
        cursor.close();
        database.close();
        return cardModels;
    }

    private void setCardTags(List<CardModel> cardModels) {
        SQLiteDatabase database = getWritableDatabase();
        for (CardModel card : cardModels) {
            String cardId = card.getId();
            String rawQuery = "SELECT * FROM " + TagsTable.TABLE_NAME + " WHERE " + TagsTable.COLUMN_CARD_ID + " = '" + cardId + "'";
            Cursor cursor = database.rawQuery(rawQuery, null);
            List<String> cardTags = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    String cardTag = TagsTable.fromCursor(cursor);
                    cardTags.add(cardTag);
                } while (cursor.moveToNext());
            }
            card.setTags(cardTags);
        }
    }

    private void setFriends(List<CardModel> cardModels) {
        SQLiteDatabase database = getWritableDatabase();
        for (CardModel card : cardModels) {
            String cardId = card.getId();
            String rawQuery = "SELECT * FROM " + FriendsTable.TABLE_NAME + " WHERE " + FriendsTable.COLUMN_CARD_ID + " = '" + cardId + "'";
            Cursor cursor = database.rawQuery(rawQuery, null);
            List<Friend> friends = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    Friend friendModel = FriendsTable.fromCursor(cursor);
                    friends.add(friendModel);
                } while (cursor.moveToNext());
            }
            card.setFriends(friends);
        }
    }


    //TODO set friends table
    //TODO get friends table

}
