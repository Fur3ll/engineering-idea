package com.example.vicktor.engineeringidea.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.vicktor.engineeringidea.model.CardModel;

public class CardsTable {

    public static final String TABLE_NAME = "Cards";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_IS_ACTIVE = "isActive";
    public static final String COLUMN_BALANCE = "balance";
    public static final String COLUMN_PICTURE = "picture";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_EYE_COLOR = "eyeColor";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_COMPANY = "company";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_ABOUT = "about";
    public static final String COLUMN_REGISTERED = "registered";
    public static final String COLUMN_FAVORITE_FRUIT = "favoriteFruit";

    public static ContentValues toContentValues(CardModel card) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, card.getId());
        cv.put(COLUMN_IS_ACTIVE, card.getIsActive());
        cv.put(COLUMN_BALANCE, card.getBalance());
        cv.put(COLUMN_PICTURE, card.getPicture());
        cv.put(COLUMN_AGE, card.getAge());
        cv.put(COLUMN_EYE_COLOR, card.getEyeColor());
        cv.put(COLUMN_NAME, card.getName());
        cv.put(COLUMN_GENDER, card.getGender());
        cv.put(COLUMN_COMPANY, card.getCompany());
        cv.put(COLUMN_EMAIL, card.getEmail());
        cv.put(COLUMN_PHONE, card.getPhone());
        cv.put(COLUMN_ADDRESS, card.getAddress());
        cv.put(COLUMN_ABOUT, card.getAbout());
        cv.put(COLUMN_REGISTERED, card.getRegistered());
        cv.put(COLUMN_FAVORITE_FRUIT, card.getFavoriteFruit());
        return cv;
    }

    public static CardModel fromCursor(Cursor cursor) {
        int idColIndex = cursor.getColumnIndex(COLUMN_ID);
        int isActiveColIndex = cursor.getColumnIndex(COLUMN_IS_ACTIVE);
        int balanceColIndex = cursor.getColumnIndex(COLUMN_BALANCE);
        int pictureColIndex = cursor.getColumnIndex(COLUMN_PICTURE);
        int ageColIndex = cursor.getColumnIndex(COLUMN_AGE);
        int eyeColorColIndex = cursor.getColumnIndex(COLUMN_EYE_COLOR);
        int nameColIndex = cursor.getColumnIndex(COLUMN_NAME);
        int genderColIndex = cursor.getColumnIndex(COLUMN_GENDER);
        int companyColIndex = cursor.getColumnIndex(COLUMN_COMPANY);
        int emailColIndex = cursor.getColumnIndex(COLUMN_EMAIL);
        int phoneColIndex = cursor.getColumnIndex(COLUMN_PHONE);
        int addressColIndex = cursor.getColumnIndex(COLUMN_ADDRESS);
        int aboutColIndex = cursor.getColumnIndex(COLUMN_ABOUT);
        int registeredColIndex = cursor.getColumnIndex(COLUMN_REGISTERED);
        int favoriteFruitColIndex = cursor.getColumnIndex(COLUMN_FAVORITE_FRUIT);

        CardModel cardModel = new CardModel();

        cardModel.setId(cursor.getString(idColIndex));
        cardModel.setIsActive(Boolean.parseBoolean(cursor.getString(isActiveColIndex)));
        cardModel.setBalance(cursor.getString(balanceColIndex));
        cardModel.setPicture(cursor.getString(pictureColIndex));
        cardModel.setAge(Integer.valueOf(cursor.getString(ageColIndex)));
        cardModel.setEyeColor(cursor.getString(eyeColorColIndex));
        cardModel.setName(cursor.getString(nameColIndex));
        cardModel.setGender(cursor.getString(genderColIndex));
        cardModel.setCompany(cursor.getString(companyColIndex));
        cardModel.setEmail(cursor.getString(emailColIndex));
        cardModel.setPhone(cursor.getString(phoneColIndex));
        cardModel.setAddress(cursor.getString(addressColIndex));
        cardModel.setAbout(cursor.getString(aboutColIndex));
        cardModel.setRegistered(cursor.getString(registeredColIndex));
        cardModel.setFavoriteFruit(cursor.getString(favoriteFruitColIndex));

        return cardModel;
    }
}
