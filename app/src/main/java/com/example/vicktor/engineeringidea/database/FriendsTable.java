package com.example.vicktor.engineeringidea.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.vicktor.engineeringidea.model.Friend;

public class FriendsTable {

    public static final String TABLE_NAME = "Friends";

    public static final String COLUMN_CARD_ID = "cardId";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";

    public static ContentValues toContentValues(String cardId, Friend friend) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CARD_ID, cardId);
        cv.put(COLUMN_ID, friend.getId());
        cv.put(COLUMN_NAME, friend.getName());
        return cv;
    }

    public static Friend fromCursor(Cursor cursor) {
        int idColIndex = cursor.getColumnIndex(COLUMN_ID);
        int friendNameColIndex = cursor.getColumnIndex(COLUMN_NAME);

        Friend friend = new Friend();

        friend.setId(Integer.valueOf(cursor.getString(idColIndex)));
        friend.setName(cursor.getString(friendNameColIndex));

        return friend;
    }
}
