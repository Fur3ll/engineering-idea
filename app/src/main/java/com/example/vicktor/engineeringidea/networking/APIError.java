package com.example.vicktor.engineeringidea.networking;

public class APIError {

    private int code;
    private String message;

    public APIError() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}