package com.example.vicktor.engineeringidea.networking;

import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Response;

public class ErrorUtils {

    public static APIError parseError(Response<?> response) {
        APIError errorResponse = null;
        String responseString = null;
        try {
            responseString = response.errorBody().string();
            Gson gson = new Gson();
            errorResponse = gson.fromJson(responseString, ErrorResponse.class).error;
        } catch (Exception e) {
            e.printStackTrace();
            errorResponse = new APIError();
            errorResponse.setCode(500);
            errorResponse.setMessage("Server error");
        }
        if (errorResponse == null && responseString != null) {
            errorResponse = new APIError();
            try {
                JSONObject data = new JSONObject(responseString);
                errorResponse.setCode(500);
                errorResponse.setMessage(data.getString(data.keys().next()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return errorResponse;
    }
}
