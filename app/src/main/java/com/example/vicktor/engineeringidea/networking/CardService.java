package com.example.vicktor.engineeringidea.networking;

import com.example.vicktor.engineeringidea.model.CardModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CardService {

    //todo look for wrong request
    @Headers("Authorization: Basic Og===")
    @GET(ApiConstance.CARD_REQUEST)
    Call<List<CardModel>> getAllCards();
}
