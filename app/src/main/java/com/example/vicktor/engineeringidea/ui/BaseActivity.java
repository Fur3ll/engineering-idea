package com.example.vicktor.engineeringidea.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.vicktor.engineeringidea.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity{

    @BindView(R.id.nav_toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title_tv)
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_toolbar);
        getLayoutInflater().inflate(getLayoutResourceId(), (ViewGroup) findViewById(R.id.rl_content), true);
        ButterKnife.bind(this);
        setupToolbar();
    }

    protected void setBaseTitle(@NonNull String text) {
        title.setText(text);
    }

    public void showToast(@NonNull String text) {
        Toast.makeText(BaseActivity.this, text, Toast.LENGTH_SHORT).show();
    }

    protected abstract int getLayoutResourceId();

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

}
