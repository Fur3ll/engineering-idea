package com.example.vicktor.engineeringidea.networking;

public final class ApiConstance {

    public static final String BASE_URL = "https://www.mocky.io/v2/";
    public static final String CARD_REQUEST = "59c92a123f0000780183f72d";
}
