package com.example.vicktor.engineeringidea.networking.request;

import com.example.vicktor.engineeringidea.networking.APIError;
import com.example.vicktor.engineeringidea.networking.ErrorUtils;
import com.example.vicktor.engineeringidea.networking.listener.RequestListener;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseRequest<T> {

    private RequestListener<T> listener;

    public void execute() {
        getCall().enqueue(callback);
    }

    public abstract Call<T> getCall();

    public Response<T> executeSync() {
        try {
            return getCall().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    Callback<T> callback = new Callback<T>() {
        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            if (listener == null) {
                return;
            }
            if (response.isSuccessful()) {
                listener.onSuccess(response.body());
            } else {
                APIError errorsModel = ErrorUtils.parseError(response);
                if (errorsModel == null) {
                    listener.onError(500, "");
                    return;
                }
                listener.onError(errorsModel.getCode(), errorsModel.getMessage());
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            if (listener != null) {
                listener.onError(500,
                        t.getMessage());
            }
        }
    };

    public void setListener(RequestListener<T> listener) {
        this.listener = listener;
    }
}
