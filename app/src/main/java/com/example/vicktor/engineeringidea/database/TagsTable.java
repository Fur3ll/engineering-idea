package com.example.vicktor.engineeringidea.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.vicktor.engineeringidea.model.CardModel;

public class TagsTable {

    public static final String TABLE_NAME = "Tags";

    public static final String COLUMN_CARD_ID = "cardId";
    public static final String COLUMN_CARD_TAGS = "cardTags";

    public static ContentValues toContentValues(String cardId, String cardTag) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CARD_ID, cardId);
        cv.put(COLUMN_CARD_TAGS, cardTag);
        return cv;
    }

    public static String fromCursor(Cursor cursor) {
        int cardTagColIndex = cursor.getColumnIndex(COLUMN_CARD_TAGS);

        return cursor.getString(cardTagColIndex);
    }
}
