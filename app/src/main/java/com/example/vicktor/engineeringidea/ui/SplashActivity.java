package com.example.vicktor.engineeringidea.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.vicktor.engineeringidea.R;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_LENGHT = 2000;
    private Handler handler = new Handler();
    private Runnable move = new Runnable() {
        @Override
        public void run() {
            openMainScreen();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        moveToNextScreen();
    }

    private void moveToNextScreen() {
        handler.postDelayed(move, SPLASH_DISPLAY_LENGHT);
    }

    private void openMainScreen() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(move);
    }

}