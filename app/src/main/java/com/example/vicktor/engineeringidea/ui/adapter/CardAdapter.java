package com.example.vicktor.engineeringidea.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vicktor.engineeringidea.R;
import com.example.vicktor.engineeringidea.model.CardModel;
import com.example.vicktor.engineeringidea.ui.CardActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardVH> {

    private List<CardModel> card;
    private Context context;

    public CardAdapter(@NonNull List<CardModel> card, Context context) {
        this.context = context;
        this.card = card;
    }

    @Override
    public CardVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new CardVH(v);
    }

    @Override
    public void onBindViewHolder(final CardVH holder, int position) {
        final CardModel item = card.get(position);
        holder.bind(card.get(position));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CardActivity.class);
                intent.putExtra(CardModel.class.getCanonicalName(), item);
                intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return card.size();
    }

    class CardVH extends RecyclerView.ViewHolder {
        @BindView(R.id.card_cv)
        CardView cardView;
        @BindView(R.id.email_tv)
        TextView email;
        @BindView(R.id.image_iv)
        ImageView image;
        @BindView(R.id.phone_tv)
        TextView phone;
        @BindView(R.id.title_tv)
        TextView title;

        public CardVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(@NonNull CardModel model) {
            title.setText(model.getName() != null ? model.getName() : "");
            email.setText(model.getEmail() != null ? model.getEmail() : "");
            phone.setText(model.getPhone() != null ? model.getPhone() : "");

            if (model.getPicture() != null && !model.getPicture().isEmpty() && !model.getPicture().equals("http://lorempixel.com/200/300/")) {
                Picasso.with(image.getContext())
                        .load(model.getPicture())
                        .fit()
                        .centerInside()
                        .into(image);
            } else {
                image.setImageResource(R.mipmap.ic_launcher);
            }
        }
    }
}
