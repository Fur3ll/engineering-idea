package com.example.vicktor.engineeringidea.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.example.vicktor.engineeringidea.R;
import com.example.vicktor.engineeringidea.model.CardModel;
import com.example.vicktor.engineeringidea.model.Friend;
import com.example.vicktor.engineeringidea.ui.adapter.FriendAdapter;
import com.greenfrvr.hashtagview.HashtagView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


@SuppressLint("Registered")
public class CardActivity extends BaseActivity {

    @BindView(R.id.til_name)
    TextInputLayout nameTil;
    @BindView(R.id.name)
    TextInputEditText nameTiet;
    @BindView(R.id.til_isActive)
    TextInputLayout isActiveTil;
    @BindView(R.id.isActive)
    TextInputEditText isActiveTiet;
    @BindView(R.id.til_id)
    TextInputLayout idTil;
    @BindView(R.id.id)
    TextInputEditText idTiet;
    @BindView(R.id.til_balance)
    TextInputLayout balanceTil;
    @BindView(R.id.balance)
    TextInputEditText balanceTiet;
    @BindView(R.id.til_age)
    TextInputLayout ageTil;
    @BindView(R.id.age)
    TextInputEditText ageTiet;
    @BindView(R.id.til_eyeColor)
    TextInputLayout eyeColorTil;
    @BindView(R.id.eyeColor)
    TextInputEditText eyeColorTiet;
    @BindView(R.id.til_gender)
    TextInputLayout genderTil;
    @BindView(R.id.gender)
    TextInputEditText genderTiet;
    @BindView(R.id.til_company)
    TextInputLayout companyTil;
    @BindView(R.id.company)
    TextInputEditText companyTiet;
    @BindView(R.id.til_email)
    TextInputLayout emailTil;
    @BindView(R.id.email)
    TextInputEditText emailTiet;
    @BindView(R.id.til_phone)
    TextInputLayout phoneTil;
    @BindView(R.id.phone)
    TextInputEditText phoneTiet;
    @BindView(R.id.til_address)
    TextInputLayout addressTil;
    @BindView(R.id.address)
    TextInputEditText addressTiet;
    @BindView(R.id.til_about)
    TextInputLayout aboutTil;
    @BindView(R.id.about)
    TextInputEditText aboutTiet;
    @BindView(R.id.til_registered)
    TextInputLayout registeredTil;
    @BindView(R.id.registered)
    TextInputEditText registeredTiet;
    @BindView(R.id.til_favoriteFruit)
    TextInputLayout favoriteFruitTil;
    @BindView(R.id.favoriteFruit)
    TextInputEditText favoriteFruitTiet;

    @BindView(R.id.contact_iv)
    ImageView cardIv;

    @BindView(R.id.hashtags)
    HashtagView hashtags;

    private CardModel card;

    private RecyclerView friendRv;
    private FriendAdapter friendAdapter;
    private List<Friend> friendsModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle();
        setupAdapterAndRecycler();
        bind();
    }

    private void bind() {
        setFields();
        setNonEditable();
    }

    private void setFields() {
        card = getIntent().getParcelableExtra(CardModel.class.getCanonicalName());
        nameTiet.setText(card.getName());
        idTiet.setText(card.getId());
        if (card.getIsActive()){
            isActiveTiet.setText(R.string.True);
        } else {
            isActiveTiet.setText(R.string.False);
        }
        balanceTiet.setText(card.getBalance());
        ageTiet.setText(Integer.toString(card.getAge()));
        eyeColorTiet.setText(card.getEyeColor());
        genderTiet.setText(card.getGender());
        companyTiet.setText(card.getCompany());
        emailTiet.setText(card.getEmail());
        phoneTiet.setText(card.getPhone());
        addressTiet.setText(card.getAddress());
        aboutTiet.setText(card.getAbout());
        registeredTiet.setText(card.getRegistered());
        favoriteFruitTiet.setText(card.getFavoriteFruit());

        if (card.getPicture() != null && !card.getPicture().isEmpty() && !card.getPicture().equals("http://lorempixel.com/200/300/") ){

            Picasso.with(CardActivity.this)
                    .load(card.getPicture())
                    .fit()
                    .centerInside()
                    .into(cardIv);
        }


        hashtags.setData(card.getTags());

        friendsModels.clear();
        friendsModels.addAll(card.getFriends());
        friendAdapter.notifyDataSetChanged();

    }

    private void setNonEditable() {
        nameTiet.setKeyListener(null);
        idTiet.setKeyListener(null);
        isActiveTiet.setKeyListener(null);
        balanceTiet.setKeyListener(null);
        ageTiet.setKeyListener(null);
        eyeColorTiet.setKeyListener(null);
        genderTiet.setKeyListener(null);
        companyTiet.setKeyListener(null);
        emailTiet.setKeyListener(null);
        phoneTiet.setKeyListener(null);
        addressTiet.setKeyListener(null);
        aboutTiet.setKeyListener(null);
        registeredTiet.setKeyListener(null);
        favoriteFruitTiet.setKeyListener(null);
    }

    private void setupAdapterAndRecycler() {
        friendRv = findViewById(R.id.friend_rv);
        friendRv.setLayoutManager(new LinearLayoutManager(this));
        friendAdapter = new FriendAdapter(friendsModels,this);
        friendRv.setAdapter(friendAdapter);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_card;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CardActivity.this,MainActivity.class));
        finish();
    }

    private void setToolbarTitle() {
        setBaseTitle(getString(R.string.card_activity_toolvar_title));
    }

}
