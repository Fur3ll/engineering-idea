package com.example.vicktor.engineeringidea.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vicktor.engineeringidea.R;
import com.example.vicktor.engineeringidea.database.DatabaseHelper;
import com.example.vicktor.engineeringidea.model.CardModel;
import com.example.vicktor.engineeringidea.networking.listener.RequestListener;
import com.example.vicktor.engineeringidea.networking.request.cardRequest.GetAllCardsRequest;
import com.example.vicktor.engineeringidea.ui.adapter.CardAdapter;
import com.example.vicktor.engineeringidea.utils.ConnectionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.empty_tv)
    TextView textView;

    private RecyclerView itemRv;
    private CardAdapter cardAdapter;
    private List<CardModel> cardModels = new ArrayList<>();
    private DatabaseHelper mDatabaseHelper = new DatabaseHelper(this);
    private ConnectionManager connectionManager;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionManager = new ConnectionManager(this);
        setToolbarTitle();
        setupAdapterAndRecycler();
        checkCardInBd(mDatabaseHelper.getCards());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void checkCardInBd(@NonNull List<CardModel> models) {
        if (models.isEmpty()) {
            requestForCard();
        } else {
            cardModels.clear();
            cardModels.addAll(mDatabaseHelper.getCards());
            cardAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void requestForCard() {
        progressBar.setVisibility(View.VISIBLE);
        if (connectionManager.isNetworkConnected()) {
            final GetAllCardsRequest request = new GetAllCardsRequest();
            request.setListener(new RequestListener<List<CardModel>>() {
                @Override
                public void onSuccess(List<CardModel> result) {
                    if (result != null) {
                        saveResultToDB(result);
                        cardModels.clear();
                        cardModels.addAll(mDatabaseHelper.getCards());
                        cardAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    progressBar.setVisibility(View.GONE);
                    showToast(message);
                }
            });
            request.execute();
        } else {
            progressBar.setVisibility(View.GONE);
            cardModels.clear();
        }
    }

    private void saveResultToDB(@NonNull List<CardModel> cardModels) {
        mDatabaseHelper.saveCards(cardModels);
    }

    private void setupAdapterAndRecycler() {
        itemRv = findViewById(R.id.card_rv);
        itemRv.setLayoutManager(new LinearLayoutManager(this));
        cardAdapter = new CardAdapter(cardModels, this);
        itemRv.setAdapter(cardAdapter);
    }

    private void setToolbarTitle() {
        setBaseTitle(getString(R.string.toolbar_title));
    }

    protected void setBaseTitle(@NonNull String text) {
        title.setText(text);
    }

}
