package com.example.vicktor.engineeringidea.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CardModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("eyeColor")
    @Expose
    private String eyeColor;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("registered")
    @Expose
    private String registered;
    @SerializedName("tags")
    @Expose
    private List<String> tags;
    @SerializedName("friends")
    @Expose
    private List<Friend> friends;
    @SerializedName("favoriteFruit")
    @Expose
    private String favoriteFruit;

    public CardModel() {
    }

    protected CardModel(Parcel in) {
        id = in.readString();
        isActive = in.readByte() != 0;
        balance = in.readString();
        picture = in.readString();
        age = in.readInt();
        eyeColor = in.readString();
        name = in.readString();
        gender = in.readString();
        company = in.readString();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
        about = in.readString();
        registered = in.readString();
        favoriteFruit = in.readString();
        friends = new ArrayList<Friend>();
        in.readTypedList(friends, Friend.CREATOR);
        tags = new ArrayList<>();
        in.readStringList(tags);
    }

    public CardModel(String id, Boolean isActive, String balance, String picture, Integer age, String eyeColor,
                     String name, String gender, String company, String email, String phone, String address,
                     String about, String registered, ArrayList<String> tags, ArrayList<Friend> friends, String favoriteFruit) {
        this.id = id;
        this.isActive = isActive;
        this.balance = balance;
        this.picture = picture;
        this.age = age;
        this.eyeColor = eyeColor;
        this.name = name;
        this.gender = gender;
        this.company = company;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.about = about;
        this.registered = registered;
        this.tags = tags;
        this.friends = friends;
        this.favoriteFruit = favoriteFruit;
    }


    public static final Creator<CardModel> CREATOR = new Creator<CardModel>() {
        @Override
        public CardModel createFromParcel(Parcel in) {
            return new CardModel(in);
        }

        @Override
        public CardModel[] newArray(int size) {
            return new CardModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getFavoriteFruit() {
        return favoriteFruit;
    }

    public void setFavoriteFruit(String favoriteFruit) {
        this.favoriteFruit = favoriteFruit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeString(balance);
        dest.writeString(picture);
        dest.writeInt(age);
        dest.writeString(eyeColor);
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeString(company);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(about);
        dest.writeString(registered);
        dest.writeString(favoriteFruit);
        dest.writeTypedList(friends);
        dest.writeStringList(tags);
    }

}
