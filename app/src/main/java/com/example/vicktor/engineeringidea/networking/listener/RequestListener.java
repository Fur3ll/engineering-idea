package com.example.vicktor.engineeringidea.networking.listener;

public interface RequestListener<T> {

    void onSuccess(T result);

    void onError(int code, String message);

}

