package com.example.vicktor.engineeringidea.ui.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vicktor.engineeringidea.R;
import com.example.vicktor.engineeringidea.model.Friend;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.FriendVH> {

    private List<Friend> friends;
    private Context context;

    public FriendAdapter(@NonNull List<Friend> friends, Context context) {
        this.context = context;
        this.friends = friends;
    }

    @Override
    public FriendVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new FriendVH(v);
    }


    @Override
    public void onBindViewHolder(final FriendVH holder, int position) {
        holder.bind(friends.get(position));
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    class FriendVH extends RecyclerView.ViewHolder {
        @BindView(R.id.friend_tv)
        TextView title;

        public FriendVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(@NonNull Friend model) {
            title.setText(model.getName() != null ? model.getName() : "");
        }
    }
}
