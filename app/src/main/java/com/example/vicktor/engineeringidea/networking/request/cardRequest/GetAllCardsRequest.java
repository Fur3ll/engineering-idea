package com.example.vicktor.engineeringidea.networking.request.cardRequest;

import com.example.vicktor.engineeringidea.model.CardModel;
import com.example.vicktor.engineeringidea.networking.NetworkHelper;
import com.example.vicktor.engineeringidea.networking.request.BaseRequest;

import java.util.List;

import retrofit2.Call;

public class GetAllCardsRequest extends BaseRequest<List<CardModel>> {

    @Override
    public Call<List<CardModel>> getCall() {
        return NetworkHelper.getInstance().getCardService().getAllCards();
    }
}
